# Utilise une image de base contenant Apache et PHP
FROM php:7.4-apache

# Copie le contenu du projet dans le conteneur
COPY . /var/www/html

# Active le module Apache pour la réécriture d'URL
RUN a2enmod rewrite

# Copie la configuration de VirtualHost dans le conteneur
COPY apache.conf /etc/apache2/sites-available/000-default.conf

# Expose le port 80 pour les connexions entrantes
EXPOSE 80

# Démarre Apache lorsque le conteneur est lancé
CMD ["apache2-foreground"]

# Installe Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer